<!DOCTYPE HTML>
<html ng-app="westdianweb">
<head>
  <title>Westdian Technologies inc.</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
  <link href="css/slider.css" rel="stylesheet" type="text/css" media="all"/>

  <script type="text/javascript" src="westdianadmin/js/angular.min.js"></script>
  <script type="text/javascript" src="westdianadmin/js/angular-route.min.js"></script>
  <script type="text/javascript" src="westdianadmin/js/ui-bootstrap-tpls-2.0.1.min.js"></script>
  <script type="text/javascript" src="js/angularscripts/index.js"></script>
  <script type="text/javascript" src="js/angularscripts/router.js"></script>
  <!--script type="text/javascript" src="js/angularscripts/slideshow.js"></script-->
  <script type="text/javascript" src="js/angularscripts/itembox.js"></script>
  <script type="text/javascript" src="js/angularscripts/categories.js"></script>
  <script type="text/javascript" src="js/angularscripts/preview.js"></script>
  <script type="text/javascript" src="js/angularscripts/contact.js"></script>

  <!--Slider -->
  <link href="css/ninja-slider.css" rel="stylesheet" type="text/css" />
  <script src="js/ninja-slider.js" type="text/javascript"></script>
</head>
<body>
  <div class="wrap">
    <div class="header">
      <div class="headertop_desc">
        <div class="call">
          <p><span>Need help?</span> call us <span class="number">0773775666</span></span></p>
        </div>
        <div class="account_desc">
          <ul>
            <li></li>
          </ul>
        </div>
        <div class="clear"></div>
      </div>
      <div class="header_top">
        <div class="logo">
          <a href="#/home"><img src="images/logo.png" alt="westdian technologies" /></a>
        </div>
        <div class="clear"></div>
      </div>
      <div class="header_bottom">
        <div class="menu">
          <ul>
            <li ng-class="{active:active=='home'}"><a href="#/home">Home</a></li>
            <li ng-class="{active:active=='about'}"><a href="#/about">About</a></li>
            <li ng-class="{active:active=='service'}"><a href="#/services">Services</a></li>
            <li ng-class="{active:active=='contact'}"><a href="#/contact">Contact</a></li>
            <div class="clear"></div>
          </ul>
        </div>
        <div class="search_box">
          <form>
            <input type="text" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}"><input type="submit" value="">
          </form>
        </div>
        <div class="clear"></div>
      </div>
      <div style="height:30px;background-color:#0088CC"></div>
      <!--slideshow></slideshow-->
      <?php include('templates/slideshow.php');?>
    </div>

    <div class="main">
      <div class="content" ng-view></div>
    </div>
  </div>

  <div class="footer">
    <div class="wrap">
      <div class="section group">
        <categories></categories>
        <div class="col_1_of_4 span_1_of_4">
          <h4>Contact</h4>
          <ul>
            <li><span>+94-077-3775666</span></li>
          </ul>
          <div class="social-icons">
            <h4>Follow Us</h4>
            <ul>
              <li><a href="https://www.facebook.com/Westdian-Technology-1608800779394480" target="_blank"><img src="images/facebook.png" alt="" /></a></li>
              <li><a href="#/twitter" target="_blank"><img src="images/twitter.png" alt="" /></a></li>
              <li><a href="#/skype" target="_blank"><img src="images/skype.png" alt="" /> </a></li>
              <li><a href="#/linkedin" target="_blank"> <img src="images/linkedin.png" alt="" /></a></li>
              <div class="clear"></div>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="copy_right">
    <p>Westdian Technologies &copy; All rights Reseverd | Design by  <a href="#">ndpdarshana</a> </p>
    </div>
  </div>
</body>
</html>
