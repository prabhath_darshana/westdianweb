<?php
  include('westdianadmin/rest/slides_get_active.php');
?>
<!--start-->
<div id="ninja-slider" ng-show="showSlideshow">
    <div class="slider-inner">
        <ul>
          <?php
            foreach ($rs as $slide) {
              echo '<li>';
              echo    '<a class="ns-img" href="images/slider/'.str_replace(" ", "", $slide['name']).'.jpg"></a>';
              echo    '<div class="caption"></div>';
              echo '</li>';
            }
            ?>
        </ul>
        <div class="navsWrapper">
            <div id="ninja-slider-prev"></div>
            <div id="ninja-slider-next"></div>
        </div>
    </div>
</div>
<!--end-->
