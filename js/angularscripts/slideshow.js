app.directive("slideshow",function(){
  return {
    templateUrl:'templates/slideshow.html'
  }
});

app.service('getSlidesService', ['$http', function($http){
  return{
      get : function(){
      var url = apiRoot + "slides_get_active.php";
      return $http.get(url);
    }
  }
}])

app.controller("slideshowCtrl", function($scope, $rootScope, getSlidesService){
  var getSlidesPromise = getSlidesService.get();
  $rootScope.slidesLoadComplete = false;
  getSlidesPromise.then(function(response){
    $scope.slides = response.data;
    $rootScope.slidesLoadComplete = true;
    console.log($rootScope.slidesLoadComplete);
  });
});
