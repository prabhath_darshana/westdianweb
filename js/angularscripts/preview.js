app.controller("previewCtrl", function($scope, $routeParams, $http, $rootScope, getCategoryItemsService){
  var itemid = $routeParams.itemid;
  var url = apiRoot + "item_get_by_itemid.php?itemid="+itemid
  $http.get(url).success(function(response){
    $scope.item = response[0];
    $rootScope.showCategory = response[0].categoryname;
    getCategoryItemsService.get(response[0].categoryid, 4)
    .then(function(response){
      $scope.categoryItems = response.data;
    });
  });


});
