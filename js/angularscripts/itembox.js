app.directive("itemBox", function(){
  return {
    templateUrl:'templates/itembox.html'
  }
});

app.service('getHomeItemsService', ['$http', function($http){
  this.get = function(scope){
    var url = apiRoot + "items_get_homepage.php";
     $http.get(url).success(function (response) {
         scope.items = response;
         console.log(response);
     });
  }
}])

app.service('getCategoryItemsService', ['$http', function($http){
  return {
      get : function(categoryid, limit){
        var url = apiRoot + "items_get_by_categoryid.php?categoryid="+categoryid+"&limit="+limit;
        return $http.get(url);
      }
  }
}])

app.controller('itemboxCtrl',function($scope, $routeParams, getHomeItemsService, getCategoryItemsService){
  if($routeParams.categoryid === undefined){
    getHomeItemsService.get($scope);
  }else{
    getCategoryItemsService.get($routeParams.categoryid, undefined)
    .then(function(response){
      $scope.items = response.data;
    });
  }
});
