app.config(function($routeProvider){
    $routeProvider.when('/home',{
      resolve:{
        slideshow : function($rootScope){
          $rootScope.showSlideshow = true;
          $rootScope.active = 'home';
        }
      },
      templateUrl: 'templates/home.html'
    }).when('/showcase',{
      resolve:{
        slideshow : function($rootScope){
          $rootScope.showSlideshow = false;
        }
      },
      templateUrl:'templates/showcase.html'
    }).when('/preview',{
      resolve:{
        slideshow : function($rootScope){
          $rootScope.showSlideshow = false;
        }
      },
      templateUrl:'templates/preview.html'
    }).when('/contact', {
      resolve:{
        slideshow : function($rootScope){
          $rootScope.showSlideshow = false;
          $rootScope.active = 'contact';
        }
      },
      templateUrl:'templates/contact.html'
    }).when('/services', {
      resolve:{
        slideshow : function($rootScope){
          $rootScope.showSlideshow = false;
        }
      },
      templateUrl:'templates/services.html'
    }).otherwise({
      redirectTo:'/home'
    })
});
