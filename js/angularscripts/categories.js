app.directive("categories",function(){
  return {
    templateUrl:'templates/categories.html'
  }
});

app.service("getCategoriesService", function($http){
  return{
    get:function(){
      return $http.get(apiRoot + "category_get_all.php");
    }
  }
});

app.controller("categoriesCtrl", function($scope, $rootScope, $http, $window, getCategoriesService){
  var getCategoriesPromise = getCategoriesService.get();
  getCategoriesPromise.then(function(response){
    $scope.categoryList = response.data;
  });

  $scope.categorySelect = function(category){
    $window.location.href = '#/showcase?categoryid='+category.categoryid;
    $rootScope.showCategory = category.name;
  }
});
