var app = angular.module("westdianweb",['ui.bootstrap', 'ngRoute']);

var apiRoot = 'westdianadmin/rest/';

app.filter('removeSpaces', [function() {
    return function(string) {
        if (!angular.isString(string)) {
            return string;
        }
        return string.replace(/[\s]/g, '');
    };
}]);
