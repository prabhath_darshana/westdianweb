app.controller("contactCtrl", function($scope, $http){
  $scope.contactFormSubmit = function(){
    $http.post(apiRoot + "mail_manager.php", $scope.contact)
    .success(function(response){
      $scope.alert = response;
      $scope.contact = undefined;
    });
  }
});
