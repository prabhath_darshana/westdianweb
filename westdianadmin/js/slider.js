app.directive("slider",function(){
  return{
    templateUrl:'slider.html'
  };
});

app.controller("slider",function($scope, $http, loginUserService, fileUploadService, updateResultService){
  $scope.alert = {type:"", msg:"", alertShow:false};
  var resultUrl = "rest/slides_get_all.php";

  $scope.uploadFiles = function($file, $invalidFile){
    $scope.alert = {type:"", msg:"", alertShow:false};
    $scope.file = $file;
  }

  $scope.uploadNewSlide = function(){
    var uploadUrl = "rest/slide_add.php";
    var userid = loginUserService.getUserid();
    var uploadData = {userid:userid, uploadText:$scope.fileName};
    var uploadPromise = fileUploadService.uploadFileToUrl($scope.file, uploadData, uploadUrl);
    uploadPromise.then(function(response){
      if(response.data.status==="success"){
        $scope.alert = {type:"success", msg:"Successfully Uploaded", alertShow:true};
      }else{
        $scope.alert = {type:"danger", msg:response.data.msg, alertShow:true};
      }
      updateResultService.get(resultUrl, $scope);
    });
  }

  $scope.updateSlide = function(slide){
    var userid = loginUserService.getUserid();
    $http.post("rest/slide_update.php", slide, {
      headers:{userid:userid}
    }).success(function(response){
      if(response.status==="success"){
        $scope.alert = {type:"success", msg:"Successfully Uploaded", alertShow:true};
      }else{
        $scope.alert = {type:"danger", msg:response.msg, alertShow:true};
      }
      updateResultService.get(resultUrl, $scope);
    });
  }

  $scope.deleteSlide = function(slideid){
    var userid = loginUserService.getUserid();
    $http.post("rest/slide_delete.php",{slideid:slideid},{
      headers:{userid:userid}
    }).success(function(response){
      if(response.status==="success"){
        $scope.alert = {type:"success", msg:"Successfully Deleted", alertShow:true};
      }else{
        $scope.alert = {type:"danger", msg:response.msg, alertShow:true};
      }
      updateResultService.get(resultUrl, $scope);
    });
  }
  updateResultService.get(resultUrl, $scope);
});
