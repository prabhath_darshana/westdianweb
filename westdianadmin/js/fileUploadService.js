app.service('fileUploadService', ['$http', 'Upload', '$timeout', 'updateResultService',
  function($http, Upload, $timeout, updateResultService){
  var uploadFileToUrl = function(file, uploadData, uploadUrl){
    file.upload = Upload.upload({
      url: uploadUrl,
      data: {file:file, data:uploadData}
    });

    return file.upload.then(function (response) {
      return response;
    });
  }

  return {uploadFileToUrl:uploadFileToUrl};
}]);

app.service('updateResultService', ['$http', function($http){
  this.get = function(url, scope){
     $http.get(url).success(function (response) {
       scope.updatedResults = response;
     });
  }
}]);
