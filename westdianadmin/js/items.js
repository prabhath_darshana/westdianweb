app.directive("items", function(){
  return{
    templateUrl:'items.html'
  };
});

app.controller("itemsCtrl", function($scope, $http, $uibModal, fileUploadService, loginUserService, updateResultService){
  var updateListUrl = "rest/items_get_all.php";
  $scope.openAddItemModal = function(){
    var modalInstance = $uibModal.open({
      animation:true,
      ariaLabelledBy:'modal-title',
      ariaDescribedBy:'modal-body',
      templateUrl:'addItemModal.html',
      controller:'addItemCtrl',
      scope: $scope,
      size:'lg',
      resolve: {
        data: function () {
          return $scope.alert;
        }
      }
    });

    modalInstance.result.then(function (alert) {
      $scope.alert = alert;
      updateResultService.get(updateListUrl, $scope);
    }, function () {
      // $log.info('Modal dismissed at: ' + new Date());
    });
  }

  $scope.itemUpdate = function(item){
    console.log("Item update");
    console.log(item);
    var userid = loginUserService.getUserid();
    $http.post("rest/item_update.php", item,{
      headers:{userid:userid}
    }).success(function(response){
      console.log(response);
      if(response.status==="success"){
        $scope.alert = {type:"success", msg:"Successfully updated", alertShow:true};
      }else{
        $scope.alert = {type:"danger", msg:response.msg, alertShow:true};
      }
      updateResultService.get(updateListUrl, $scope);
    });
  }

  $scope.itemDelete = function(item){
    console.log("item delete")
    console.log(item);
    var userid = loginUserService.getUserid();
    $http.post("rest/item_delete.php",
    {itemid:item.itemid},{
      headers:{userid:userid}
    }).success(function(response){
      console.log(response);
      if(response.status==="success"){
        $scope.alert = {type:"success", msg:"Successfully deleted", alertShow:true};
      }else{
        $scope.alert = {type:"danger", msg:response.msg, alertShow:true};
      }
      updateResultService.get(updateListUrl, $scope);
    });
  }

  updateResultService.get(updateListUrl, $scope);
});

app.controller("addItemCtrl", function($scope, $uibModalInstance,
  fileUploadService, loginUserService, updateResultService, getActiveCategories){
  var getCategoryiesPromise = getActiveCategories.get();
  getCategoryiesPromise.then(function(response){
    $scope.categories = response.data;
  });

  $scope.uploadFiles = function($file, $invalidFile){
    $scope.alert = {type:"", msg:"", alertShow:false};
    $scope.file = $file;
  }

  $scope.addItem = function(){
    var uploadUrl = "rest/item_add.php";
    var userid = loginUserService.getUserid();
    var data = {userid:userid,
                itemName:$scope.itemName,
                itemPrice:$scope.itemPrice,
                itemActive:$scope.itemActive,
                itemShowHomepage:$scope.itemShowHomepage,
                itemCategory:$scope.selectedCategory,
                itemDescription:$scope.description
              };
    var uploadPromise = fileUploadService.uploadFileToUrl($scope.file, data, uploadUrl);
    uploadPromise.then(function(response){
      if(response.data.status==="success"){
        $scope.alert = {type:"success", msg:"Successfully Uploaded", alertShow:true};
      }else{
        $scope.alert = {type:"danger", msg:response.data.msg, alertShow:true};
      }
      $uibModalInstance.close($scope.alert);
    });
  }

  $scope.cancel = function(){
    $uibModalInstance.dismiss('cancel');
  }
});
