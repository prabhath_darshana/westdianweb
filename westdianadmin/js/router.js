var app = angular.module("westdianadmin",['ngRoute','ui.bootstrap','ngFileUpload']);

app.config(function($routeProvider){
    $routeProvider.when('/login',{
        templateUrl: 'login.html'
    }).when('/home',{
        resolve: {
          authenticate:function($location, loginUserService){
              if(loginUserService.getUserid() == undefined){
                  $location.path('/login')
              }
          }
        },
        templateUrl:'home.html'
    }).otherwise({
        redirectTo:'/login'
    })
});
