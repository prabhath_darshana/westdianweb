app.controller("sidepanel", function($scope, $rootScope){
  $rootScope.content = "slider";

  $scope.slider = function(){
    $rootScope.content = "slider";
  }

  $scope.items = function(){
    $rootScope.content = "items";
  }

  $scope.categories = function(){
    $rootScope.content = "categories";
  }
});

app.filter('removeSpaces', [function() {
    return function(string) {
        if (!angular.isString(string)) {
            return string;
        }
        return string.replace(/[\s]/g, '');
    };
}]);
