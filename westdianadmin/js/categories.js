app.directive("categories", function(){
  return{
    templateUrl:'categories.html'
  };
});

app.service("getActiveCategories", function($http){
  return {
      get: function(){
         return $http.get("rest/categories_get_active.php");
        }
      }
});

app.controller("categoryCtrl", function($scope, $http, loginUserService, updateResultService){
  $scope.alert = {type:"", msg:"", alertShow:false};

  $scope.addCategory = function(){
    var addCategoryUrl = "rest/category_add.php";
    var userid = loginUserService.getUserid();
    var data = {userid:userid, categoryName:$scope.categoryName, categoryActive:$scope.categoryActive};

    $http.post(addCategoryUrl, data, {
      headers:{userid:userid}
    }).success(function(response){
      console.log(response);
      if(response.status==="success"){
        $scope.alert = {type:"success", msg:"Successfully added", alertShow:true};
      }else{
        $scope.alert = {type:"danger", msg:response.data.msg, alertShow:true};
      }
      updateResultService.get("rest/category_get_all.php", $scope);
    }).error(function(){

    });
  }

  $scope.categoryActiveList = {};
  $scope.updateActive = function(categoryid){
    var userid = loginUserService.getUserid();
    var active = $scope.categoryActiveList[categoryid];
    var data = {userid:userid, categoryid:categoryid, categoryActive:active};
    $http.post("rest/category_update.php", data, {
      headers:{userid:userid}
    }).success(function(response){
      if(response.status==="success"){
        $scope.alert = {type:"success", msg:"Successfully updated", alertShow:true};
      }else{
        $scope.alert = {type:"danger", msg:response.data.msg, alertShow:true};
      }
    });
  }

  $scope.deleteCategory = function(categoryid){
    var userid = loginUserService.getUserid();
    var data = {userid:userid, categoryid:categoryid}
    $http.post("rest/category_delete.php", data, {
      headers:{userid:userid}
    }).success(function(response){
      if(response.status==="success"){
        $scope.alert = {type:"success", msg:"Successfully updated", alertShow:true};
      }else{
        $scope.alert = {type:"danger", msg:response.msg, alertShow:true};
      }
      updateResultService.get("rest/category_get_all.php", $scope);
    });
  }
  updateResultService.get("rest/category_get_all.php", $scope);
});
