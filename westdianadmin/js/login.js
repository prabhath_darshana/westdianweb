// var app= angular.module("westdianadmin",['ui.bootstrap']);
app.factory("loginUserService", function($rootScope){
    var userid='';
    return{
        setUserid : function(newUserid){
            if(newUserid != undefined){
                $rootScope.showlogoff = true;
            }else{
                $rootScope.showlogoff = false;
            }
            this.userid = newUserid;
        },
        getUserid : function(){
            return this.userid;
        }
    };
});

app.controller("loginCtrl", function($scope, $http, $window, loginUserService){
  $scope.showErrot = false;
  $scope.login = function(){
    $http({
      method  : 'POST',
      url     : 'rest/login.php',
      data    : {
        username : $scope.user.username,
        password : $scope.user.password
      }, //forms user object
      headers : {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function(response) {
      if (response.status === 'success') {
        loginUserService.setUserid(response.userid);
        $window.location.href = '#/home';
        $scope.showErrot = false;
      } else {
        $scope.showErrot = true;
      }
    });
  }
});
