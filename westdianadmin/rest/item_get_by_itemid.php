<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

require_once('lib/db/dbConnection.php');

$itemid = $_GET['itemid'];

$sql = "SELECT  item.itemid,
                item.name,
                item.price,
                item.image,
                item.description,
                cat.name as categoryname,
                cat.categoryid
 FROM item INNER JOIN categories cat ON cat.categoryid = item.categoryid WHERE itemid=?";
$rs = getDataWithParam($sql, array($itemid));

echo json_encode($rs);
?>
