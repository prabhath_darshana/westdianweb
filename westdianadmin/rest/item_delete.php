<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

require_once('lib/db/dbConnection.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$response = [];
if(isset($request)){
  try {
    $itemid = $request->itemid;
    if(isset($itemid)){
      $sql = "DELETE FROM item WHERE itemid=?";
      deleteData($sql, array($itemid));
      $response = array("status"=>"success");
    }else{
      $response = array("status"=>"failed", "msg"=>"Data is not set");
    }
  }catch(Exception $e){
    $response = array("status"=>"failed", "msg"=>$e->getMessage());
  }
}
echo json_encode($response);
?>
