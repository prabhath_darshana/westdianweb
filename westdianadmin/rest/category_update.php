<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

require_once('lib/db/dbConnection.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$response = [];
if(isset($request)){
  try {
    $categoryid = $request->categoryid;
    $active = isset($request->categoryActive) && $request->categoryActive == 'true' ? 1 : 0;

    if(isset($categoryid)){
      $sql = "UPDATE categories SET active=? WHERE categoryid=?";
      $rs = updateData($sql, array($active,$categoryid));
      if($rs>0){
        $response = array("status"=>"success");
      }else{
        $response = array("status"=>"failed", "msg"=>"Database update failed");
      }
    }else{
      $response = array("status"=>"failed", "msg"=>"Data is not set");
    }
  }catch(Exception $e){
    $response = array("status"=>"failed", "msg"=>$e->getMessage());
  }
}
echo json_encode($response);
?>
