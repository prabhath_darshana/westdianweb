<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

include('config.php');

$db = new PDO('mysql:host='.$host.';dbname='.$database.';charset=utf8mb4', $username, $password);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

/**
*This function can only used to retrive data using SELECT query without parameters
*/
function getData($sql){
  global $db;
  $stmt = $db->query($sql);
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

/**
*This function can only used to retrive data using SELECT query with parameters
*/
function getDataWithParam($sql, $param){
  global $db;
  $stmt = $db->prepare($sql);
  $stmt->execute($param);
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

/**
*This function can use to insert executions
*/
function insertData($sql, $param){
  global $db;
  $stmt = $db->prepare($sql);
  $stmt->execute($param);
  return $db->lastInsertId();
}

/**
*This function can use to update executions
*/
function updateData($sql, $param){
  global $db;
  $stmt = $db->prepare($sql);
  $stmt->execute($param);
  return $stmt->rowCount();
}

/**
*This function can use to delete executions
*/
function deleteData($sql, $param){
  global $db;
  $stmt = $db->prepare($sql);
  $stmt->execute($param);
}

// echo json_encode(getData("SELECT * FROM users"));
?>
