<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

require_once('lib/db/dbConnection.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$response = [];
if(isset($request)){
  try {
    $slideid = $request->slideid;
    $active = $request->active;
    $slideorder = $request->slide_order;

    if(isset($slideid)){
      $sql = "UPDATE slider SET active=?, slide_order=? WHERE slideid=?";
      $rs = updateData($sql, array($active,$slideorder,$slideid));
      if($rs>0){
        $response = array("status"=>"success");
      }else{
        $response = array("status"=>"failed", "msg"=>"Database update failed");
      }
    }else{
      $response = array("status"=>"failed", "msg"=>"Data is not set");
    }
  }catch(Exception $e){
    $response = array("status"=>"failed", "msg"=>$e->getMessage());
  }
}
echo json_encode($response);
?>
