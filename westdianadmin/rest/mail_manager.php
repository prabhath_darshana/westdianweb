<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

require_once('lib/db/dbConnection.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$response = [];
if(isset($request)){
  $to = "info@westdiantechnologies.com";
  $subject = $request->name . " : " .$request->subject;
  $fromemail = $request->email;
  $message = $request->msg;

  if(isset($request)){
    $sql = "INSERT INTO messages (subject,fromemail,msgbody,unread) VALUES (?,?,?,?)";
    $rs = insertData($sql, array($subject,$fromemail,$message,1));
    if($rs>0){
      $response = array("status"=>"success");
      $headers = "From: ".$fromemail;
      mail($to,$subject,$message,$headers);
    }else{
      $response = array("status"=>"failed", "msg"=>"Database insertion failed");
    }
  }else{
    $response = array("status"=>"failed", "msg"=>"Data is not set");
  }
}
echo json_encode($response);
?>
