<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

require_once('lib/db/dbConnection.php');

$sql = "SELECT * FROM categories WHERE active=1 ORDER BY name ASC";
$rs = getData($sql);

echo json_encode($rs);
?>
