<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

require_once('lib/db/dbConnection.php');

// $postdata = file_get_contents("php://input");
// $request = json_decode($postdata);

$categoryid = $_GET['categoryid'];//$request->categoryid;
$limit = $_GET['limit'];
$sqlLimiter = "";
if($limit > 0){
  $sqlLimiter = "LIMIT ".$limit;
}

$sql = "SELECT * FROM item WHERE active=1 AND categoryid=? ".$sqlLimiter;
$rs = getDataWithParam($sql, array($categoryid));

echo json_encode($rs);
?>
