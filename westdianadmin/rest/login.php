<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

require_once('lib/db/dbConnection.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);


$sql = "SELECT userid, username FROM users WHERE username=? AND password=?";
$rs = getDataWithParam($sql, array($request->username,$request->password));

$response = [];
if(count($rs)>0){
  $response = array("status"=>"success", "userid"=>$rs[0]['userid']);
}else{
  $response = array("status"=>"failed");
}

echo json_encode($response);
?>
