<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

require_once('lib/db/dbConnection.php');

$response = [];
if(isset($_POST['data'])){
  try {
    $uploadText = $_POST['data']['uploadText'];
    $userid = $_POST['data']['userid'];
    $moved = move_uploaded_file($_FILES['file']['tmp_name'],"../../images/slider/".str_replace(" ", "", $uploadText).".jpg");

    if($moved){
      $sql = "INSERT INTO slider (name, slide_order, active, userid) VALUES (?,?,?,?)";
      $rs = insertData($sql, array($uploadText, 1, 1, $userid));
      if($rs>0){
        $response = array("status"=>"success");
      }else{
        $response = array("status"=>"failed", "msg"=>"Database insertion failed");
      }
    }else{
      $response = array("status"=>"failed", "msg"=>"File saving failed");
    }
  }catch(Exception $e){
    $response = array("status"=>"failed", "msg"=>$e->getMessage());
  }
}

echo json_encode($response);
?>
