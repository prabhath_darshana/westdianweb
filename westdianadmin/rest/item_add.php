<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

require_once('lib/db/dbConnection.php');

$response = [];
if(isset($_POST['data'])){
  try {
    $uploadText = $_POST['data']['itemName'];
    $userid = $_POST['data']['userid'];
    $price = $_POST['data']['itemPrice'];
    $itemCategory = $_POST['data']['itemCategory'];
    $active = isset($_POST['data']['itemActive']) && $_POST['data']['itemActive'] == 'true' ? 1 : 0;
    $showHomepage = isset($_POST['data']['itemShowHomepage']) && $_POST['data']['itemShowHomepage'] == 'true' ? 1 : 0;
    $fileName = "item_".str_replace(" ", "", $uploadText).".jpg";
    $description = $_POST['data']['itemDescription'];

    $moved = move_uploaded_file($_FILES['file']['tmp_name'],"../../images/items/".$fileName);

    if($moved){
      $sql = "INSERT INTO item (name, price, image, userid, active, show_homepage, categoryid, description) VALUES (?,?,?,?,?,?,?,?)";
      $rs = insertData($sql, array($uploadText, $price, 'images/items/'.$fileName, $userid, $active, $showHomepage, $itemCategory, $description));
      if($rs>0){
        $response = array("status"=>"success");
      }else{
        $response = array("status"=>"failed", "msg"=>"Database insertion failed");
      }
    }else{
      $response = array("status"=>"failed", "msg"=>"File saving failed");
    }
  }catch(Exception $e){
    $response = array("status"=>"failed", "msg"=>$e->getMessage());
  }
}
echo json_encode($response);
?>
